
# Comment contribuer à geocvdl

## mode operatoire pour l'ajout d'un couche.

### Créer une issue et une branche à partir de cette issue.

Issue: pour spécifier la couche geographique qui va être ajouter. Elle portera un numero.
La branche spécifique sera créer à partir de dev.

Dans Rstudio, se positionner dans cette branche spécifique

### Récupération de la donnée

Dans *data-raw/DATASET.R*:
créer le code permettant de lire la couche géographique sur le réseau, suivie de:
``` r
usethis::use_data(nom_de_ma_couche,version=3,overwrite=TRUE)
```

Autant que possible créer des variables globales pour les chemins d'accès 
Ne pas oublier de mettre l'appel au library nécessaire en haut du script.

### Documentation de la couche géographique

Dans *devstuff_history.R*:
Rajouter dans la partie: 

creation de la documentation de la donnee

usethis::use_r("doc_nom_de_ma_couche")

Remplir le fichier *doc_nom_de_ma_couche*:

``` r
#' nom_de_ma_couche
#' le descriptif de ma couche
#' maj socle : jj/mm/aaaa
#' date chargement : jj/mm/aaaa
#' @docType data
"nom_de_ma_couche"
```
### Ajout de la vignette
Dans *devstuff_history.R*:
dans la partie vignettes, rajouter:
usethis::use_vignette("nom_de_ma_couche")

Dans le fichier créer, compléter la partie suivante avec la couche géographique intégrée 

```{r setup}
library(geocvdl)
visualisation(nom_de_ma_couche)
```

### Check 

Dans Rstudio, checker le package.
Vérifier qu'il y a 
``` r
# 0 errors v | 0 warnings v | 0 notes v
```
Cliquer sur Install and Restart.

### Commit

Réaliser un commit sur la branche spécifique, en précisant dans le commit #XX où XX est le numéro de l'issue, ce qui permettra de relier ce commit à l'issue adéquat.

### Merge Request

Dans *Gitlab*
- aller dans la page Branches
- se positionner dans la branche spécifique
- cliquer sur Create merge request
- vérifier que le merge request est bien de la branche dev_issueXX vers la branche dev.
- Assigner quelqu'un.
- Cliquer sur Submit merge request.

### Pour finir

Une fois la branche mergée:
- s'assurer que celle ci a bien été supprimée.
- aller dans l'issue en question et la clore.



