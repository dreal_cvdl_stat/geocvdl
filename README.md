# geocvdl

Ce package a pour objectif d’être une banque de couches géographiques concernant la région Centre-Val de Loire.

Le site web de présentation du package est :  

- pour la version validée (master) : https://dreal_cvdl_stat.gitlab.io/geocvdl/index.html  
- pour la version en développement (dev) : https://dreal_cvdl_stat.gitlab.io/geocvdl/dev/index.html  

Le but de {geocvdl} est de faciliter l'accès aux couches géographiques hors réseau.

Attention, projet encore en développement actif.

## Installation
Pour installer le package : 

``` r
# install.packages("remotes")
remotes::install_gitlab("dreal_cvdl_stat/geocvdl")
```
