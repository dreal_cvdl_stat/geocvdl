---
title: "HYDRO_R24"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{HYDRO_R24}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(geocvdl)
visualisation(HYDRO_R24)
```
